/*!
 * Data model class, contains all hands free preference configurations.
 * Host application pass instance of KSAdvanceSettingModel to setAdvanceSetting method of SDK to save hands free configuration on server.
 */
#import <Foundation/Foundation.h>

@interface KSAdvanceSettingModel : NSObject
@property (nonatomic) BOOL handsFreeEnable;

@end

