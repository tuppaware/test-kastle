//
//  KSAuthorizedUserResponse.h
//  KastleSDK
//
//  Copyright © 2019 Kastle. All rights reserved.
//

#import "KSServiceResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface KSAuthorizedUserResponse : KSServiceResponse

@end

NS_ASSUME_NONNULL_END
