//
//  KSBLEConstants.h
//  KastleSDK
//

typedef NS_ENUM(NSInteger, KSEnterpriseModeOfOperation) {
    KSEnterpriseModeOfOperationHandsFree = 1,
    KSEnterpriseModeOfOperationForeground = 4
};

typedef NS_ENUM(NSInteger, KSReaderType) {
    
    KSReaderTypeUnknown = 0,
    KSReaderTypeElevator,
    KSReaderTypeInterior,
    KSReaderTypePerimeter,
    KSReaderTypeOccupancy,
    KSReaderTypeGarageEntry,
    KSReaderTypeGarageExit,
    KSReaderTypeTurnstile,
    KSReaderTypeBathroom,
    KSReaderTypeStairway,
    KSReaderTypeGarageEntryWithLoop,
    KSReaderTypeGarageExitWithLoop,
    KSReaderTypeWiredBeacon,
    KSReaderTypeAutomatic,
    KSReaderTypeApartment,
    KSReaderTypeWireless,
    KSReaderTypeOffline,
    KSReaderTypeNonBLE
    
};

typedef NS_ENUM(NSUInteger, KSReaderZone)
{
    KSReaderZonePresence = 0,
    KSReaderZoneFastScan,
    KSReaderZoneAction,
    KSReaderZoneSureShot
};


typedef NS_ENUM(NSInteger, KSReaderModeOfOperation)
{
    KSReaderModeOfOperationNone = -1,
    KSReaderModeOfOperationHandsfree = 0,
    KSReaderModeOfOperationWave,
    KSReaderModeOfOperationKnockKnock,
    KSReaderModeOfOperationAppTap,
    KSReaderModeOfOperationAppTapWithPin,
    KSReaderModeOfOperationAppTapBiometric,
    KSReaderModeOfOperationRemoteUnlock
};
