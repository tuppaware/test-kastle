//
//  KSBLEDelegate.h
//  KastleSDK
//

#import <Foundation/Foundation.h>

@class KSBLEReader;
@class KSBLEError;

/**
 This is the protocol used to sending the UI data to host appliaction
 */
@protocol KSBLEDelegate <NSObject>
@required
/**
 This method will be consumed by Host application will contain all the data to be shown on host application for readerUI.
 
 @param reader KSBLEReader class carrying data to be shown on host application
 */

- (void)bleReaderDidReceive:(KSBLEReader *)reader;

- (void)bleDidFailWithError:(KSBLEError *)error;

@end
