//
//  KSBLEError.h
//  KastleSDK
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString *const KSBLEErrorDomain;


typedef NS_ENUM(NSInteger, KSBLEErrorCode) {
    KSBLENoError = 0,
    KSBLEBluetoothPoweredOff,
    KSBLELocationServiceOff,
    KSBLEAutomaticTimeZoneNotSet,
    KSBLEDeviceUnlockFailed,
    KSBLEReadersOutOfRange,
    KSBLEBluetoothUsageNotAuthorized,
    KSBLEServiceInitializationFailed
};


/**
 Data model class, contains error code and error message of ble failure.
 */
@interface KSBLEError : NSObject

@property (nonatomic, copy, readonly) NSString *message;

@property (nonatomic, assign, readonly) KSBLEErrorCode errorCode;

+ (instancetype)errorWithMessage:(NSString *)message code:(NSInteger)code;

- (instancetype)init NS_UNAVAILABLE;




@end

NS_ASSUME_NONNULL_END
