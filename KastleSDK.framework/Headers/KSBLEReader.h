//
//  KSBLEReader.h
//  KastleSDK
//

#import "KSReader.h"

NS_ASSUME_NONNULL_BEGIN

@interface KSBLEReader : KSReader

@property (nonatomic, copy, readonly) NSNumber *rssi;
@property (nonatomic, assign, readonly) KSReaderZone readerZone;
@property (nonatomic, copy, readonly) NSString *readerZoneLabel;
@property (nonatomic, assign, readonly ,getter = hasUserAction) BOOL userAction;
@property (nonatomic, assign, readonly) BOOL isDoorOpen;

- (instancetype)initWithReaderID:(NSInteger)readerID;

@end


NS_ASSUME_NONNULL_END
