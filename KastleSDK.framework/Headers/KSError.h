//
//  KSError.h
//  KastleSDK
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

    // Error reporting
extern NSString *const KSErrorDomain;

typedef NS_ENUM(NSInteger, KSErrorCode) {
    KSErrorNoInternetConnection = 9000,
    KSErrorNetowrkError = 9001,
    KSErrorResponseFailure = 9002,
    KSErrorAuthFailure = 9003,
    KSErrorInvalidRequest = 9004,
    KSErrorServiceNotSubscribed = 9005,
    KSErrorRemoteUnlockError = 9006,
    KSErrorRefreshReaderFailure = 9008,
    KSErrorInvalidToken = 9009
};


/**
 A class for representing error for Kastle related operations
 code: contains error code.
 message: contains error message.
 */

@interface KSError : NSObject

/// Code represent
@property (nonatomic, readonly) KSErrorCode code;

@property (nonatomic, strong, readonly) NSString *message;


@end

NS_ASSUME_NONNULL_END
