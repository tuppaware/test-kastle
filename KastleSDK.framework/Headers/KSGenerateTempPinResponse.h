/*!
 *  API response class to retrieve response of generateTempPin API call.
 */
#import "KSServiceResponse.h"
NS_ASSUME_NONNULL_BEGIN
@interface KSGenerateTempPinResponse : KSServiceResponse
@end
NS_ASSUME_NONNULL_END
