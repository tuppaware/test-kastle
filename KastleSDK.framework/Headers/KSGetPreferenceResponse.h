#import "KSUserPreferenceModel.h"
#import "KSServiceResponse.h"

NS_ASSUME_NONNULL_BEGIN
/*!
 * API response data class of getPreference API.Object of this class returns object of KSUserPreferenceModel class as additional response data.
 */
@class KSGetPreferenceData;

@interface KSGetPreferenceData : NSObject
@property (nonatomic,strong) KSUserPreferenceModel *userPreference;
@end


/*!
 * API response class to retrieve response of getPreference api call. It contains object of KSGetPreferenceData class.
 */
@interface KSGetPreferenceResponse : KSServiceResponse
@property (nullable, strong, readonly) KSGetPreferenceData *data;
@end

NS_ASSUME_NONNULL_END
