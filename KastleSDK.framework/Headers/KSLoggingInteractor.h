//A class to prepare log and export to host application.
#import <UIKit/UIKit.h>
#import "KSLoggingInteractor.h"

NS_ASSUME_NONNULL_BEGIN

extern NSString *const KSPDFLogFileName;

typedef NS_ENUM(NSInteger, KSGeneratePDFErrorCode) {
    KSGeneratePDFNoError = 0,
    KSGeneratePDFFileError,
    KSGeneratePDFNoDataAvailableError,
    KSGeneratePDFExternalStorageUnavilableError
};

@interface KSPDFDataModel : NSObject
@property (nullable, nonatomic, readonly) NSString *filePath;
@property (nonatomic, readonly) KSGeneratePDFErrorCode errorCode;
@property (nullable, nonatomic, readonly) NSString *message;
@end



@interface KSLoggingInteractor : NSObject

/**
 This completion handler will generate the log file and return instance of KSPDFModel on completion.
 
 @param completionHandler Completion handler to perform operation.
 */
- (void)exportLogWithCompletionHandler:(void (^) (KSPDFDataModel* pdfModel))completionHandler;

@end

NS_ASSUME_NONNULL_END
