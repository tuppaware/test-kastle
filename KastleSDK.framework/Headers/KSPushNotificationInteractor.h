//
//  KSPushNotificationInteractor.h
//  KastleSDK
//
//  Copyright © 2019 Kastle. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KSPushNotificationInteractor : NSObject

/**
 Process the VoIP token.

 @param voIPDeviceToken VoIP push token.
 */
- (void)processVoIPDeviceToken:(NSString *)voIPDeviceToken;


/**
 Handles the VoIP notification payload specific to Kastle.
 
 @param voIPMessage A dictionary payload received in notification
 @param completionHandler The block to be executed by sdk when finished processing the notification payload.
 you must assign this completion handler a completion handler of PKPushRegistry's delegate method (pushRegistry:didReceiveIncomingPushWithPayload:forType:withCompletionHandler:)
 */
- (void)processVoIPMessage:(NSDictionary *)voIPMessage completionHandler:(dispatch_block_t)completionHandler;


- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
