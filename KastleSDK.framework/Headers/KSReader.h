#import "KSReader.h"
#import "KSBLEConstants.h"

NS_ASSUME_NONNULL_BEGIN

@interface KSReader : NSObject

/// Reader ID
@property (nonatomic, copy) NSNumber *readerID;

/// Type of a reader
@property (nonatomic, assign, readonly) KSReaderType readerType;

/// Mode of operation for a reader
@property (nonatomic, assign, readonly) KSReaderModeOfOperation modeOfOperation;

/// Reader description
@property (nonatomic, strong, readonly) NSString *readerDescription;

/// Designator for a reader
@property (nonatomic, strong, readonly) NSString *readerDesignator;

/// A string to respresent the type of a reader.
@property (nonatomic, copy, readonly) NSString *readerTypeLabel;

/// A string to respresent the mode of operation for a reader.
@property (nonatomic, copy, readonly) NSString *modeOfOperationLabel;


- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithReaderID:(NSInteger)readerID;

@end


NS_ASSUME_NONNULL_END

