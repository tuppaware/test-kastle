//
//  KSRefreshReadersResponse.h
//  KastleSDK
//
//  Copyright © 2019 Kastle. All rights reserved.
//

#import "KSServiceResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface KSRefreshReadersResponse : KSServiceResponse

/// Whether the reader data successfully refreshed or not
@property(nonatomic, readonly) BOOL isSuccess;

@end

NS_ASSUME_NONNULL_END
