#import "KSValidateUser.h"

NS_ASSUME_NONNULL_BEGIN

/*!
 * Data Model class to get requested parameters for registerUserWithoutPin API. Required parametes are email and mobileNumber.
 */
@interface KSRegisterUserWithoutPin : NSObject
@property (nonatomic, strong, readonly) NSString *email;
@property (nonatomic, strong, readonly) NSString *mobilenumber;
@property (nonatomic, strong, readonly) NSString *userIdentityGUID;
- (instancetype)initWithEmail:(NSString *)email mobileNumber:(NSString *)mobilenumber userIdentityGUID:(NSString *)userIdentityGUID;
@end


@interface KSRegisterUserWithPin : NSObject

@property (nonatomic, strong, readonly) NSString *temporaryPin;

- (instancetype)initWithTemporaryPin:(NSString *)tempPin;
@end


NS_ASSUME_NONNULL_END
