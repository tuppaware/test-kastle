#import "KSServiceResponse.h"

@class KSRegisterUserWithoutTempPinResponse;
@class KSRegisterUserData;

NS_ASSUME_NONNULL_BEGIN

/*!
 * API response data class of register API's. Object of this class returns firstName,lastName and profileImageURl of user as additional response data.
 */
@interface KSRegisterUserData : NSObject
@property (nonatomic, strong, readonly) NSString *firstName;
@property (nonatomic, strong, readonly) NSString *lastName;
@property (nonatomic, strong, readonly) NSString *profileImageURL;
@end


@interface KSRegisterUserResponse : KSServiceResponse

@property(nullable, nonatomic, strong, readonly) KSRegisterUserData *data;

@end

/*!
* API response class to retrieve response of registerUserWithoutPin API call. It returns object of KSRegisterUserData data.
*/
@interface KSRegisterUserWithoutTempPinResponse : KSServiceResponse
@property(nullable, nonatomic, strong, readonly) KSRegisterUserData *data;
@end

NS_ASSUME_NONNULL_END
