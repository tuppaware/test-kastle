/*!
 * Data Model class, contains all required properties to remote unlock doors.
 *  Object of KSRemoteUnlock is pass to remoteUnlock method of sdk to unlock door.
 */
#import <Foundation/Foundation.h>
#import "KSBLEConstants.h"
NS_ASSUME_NONNULL_BEGIN

@interface KSRemoteUnlock : NSObject
@property (nonatomic, copy, readonly) NSNumber *readerID;
@property (nonatomic, copy, readonly) NSString *readerDesignator;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithReaderID:(NSNumber *)readerID readerDesignator:(NSString *)readerDesignator;

@end

NS_ASSUME_NONNULL_END

