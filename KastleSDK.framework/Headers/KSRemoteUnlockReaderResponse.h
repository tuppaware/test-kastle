#import "KSServiceResponse.h"
#import "KSReader.h"

NS_ASSUME_NONNULL_BEGIN

/*!
 * API response class to retrieve response of fetchRemoteReaders API call. Object of this class returns array of type KSRemoteUnlock as response data.
 */
@interface KSRemoteUnlockReaderResponse : KSServiceResponse
@property(nullable, nonatomic, strong, readonly) NSArray<KSReader *> *data;
;
@end

NS_ASSUME_NONNULL_END

