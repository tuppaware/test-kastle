//
//  KSServiceManagerProtocol.h
//  KastleSDK
//

#import <Foundation/Foundation.h>

@class KSAuthorizedUserResponse;
@class KSAdvanceSettingModel;
@class KSGenerateTempPinResponse;
@class KSGetPreferenceResponse;
@class KSRefreshReadersResponse;
@class KSRegisterUserResponse;
@class KSRegisterUserWithPin;
@class KSRegisterUserWithoutPin;
@class KSRegisterUserWithoutTempPinResponse;
@class KSRemoteUnlock;
@class KSRemoteUnlockReaderResponse;
@class KSServiceResponse;
@class KSSetAwakeResponse;
@class KSSetPreferenceResponse;
@class KSUnlatchDoorResponse;
@class KSUserPreferenceModel;
@class KSValidateUser;
@class KSValidateUserResponse;
@class KSError;

/**
 *  KSServiceManagerInteractor
 *
 *  Discussion:
 *   A class that defines the network related services behavior specific to Kastle.
 *    eg. user registration, fetch user's reader list etc.
 */

NS_ASSUME_NONNULL_BEGIN

@interface KSServiceManagerInteractor : NSObject

- (instancetype)init NS_UNAVAILABLE;

    /// Validates whether the current user is an authorized user
    /// @param completionHandler Invokes on competion
- (void)validateAuthorizedUserWithCompletionHandler:(void (^) (KSAuthorizedUserResponse *response) )completionHandler;

/*!
 This method is used to send email id and phone number for registration process.
 
 @param validateUser user validation model
 
 @param completionHandler Invokes completionHandler() on the completion
 */
- (void)validateUser:(KSValidateUser *)validateUser completionHandler:(void (^) (KSValidateUserResponse *response))completionHandler;

/*!
 This method will be used for registration for the enterprise using "Plai" app for data entry.
 
 @param registerUserWithPin user registration model
 
 @param completionHandler Invokes completionHandler() on the completion
 */
- (void)registerUserWithPin:(KSRegisterUserWithPin *)registerUserWithPin completionHandler:(void (^) (KSRegisterUserResponse *response))completionHandler;

/*!
 *This method is used to register user without requiring temp pin. It return instance of KSRegisterUserWithoutTempPinResponse.
 
 @param registerUserWithPin request which contain email id and mobile number of user.
 @param completionHandler completion handler to perform API operation.
 */
- (void)registerUserWithoutPin:(KSRegisterUserWithoutPin *)registerUserWithPin completionHandler:(void (^) (KSRegisterUserWithoutTempPinResponse *response))completionHandler;


/*!
 *This method is used to make api call to generate temp pin. It return instance of KSGenerateTempPinResponse class.
 
 @param completionHandler Handler to perform API action.
 */
- (void)generateTempPin:(void (^) (KSGenerateTempPinResponse *response))completionHandler;

/*!
 *This method is used to make api call to save perference of server. It return instance of KSSetPreferenceResponse.
 * *-This method will not save handsFree mode preference. To save hands Free, use setHandsFree method -*.
 
 @param userPreference User preference model with all updated values.
 @param completionHandler Handler to perform API operation.
 */
- (void)setUserPreference:(KSUserPreferenceModel *)userPreference completionHandler:(void (^) (KSSetPreferenceResponse *response))completionHandler;

/*!
 *This method is used to make api call to save hands free state on server. It return instance of KSSetPreferenceResponse.
 @param userAdvanceSettingPreference model to save advance user prefrence.
 @param completionHandler Handler to perform API operation.
 */
- (void)setAdvanceSetting:(KSAdvanceSettingModel *)userAdvanceSettingPreference completionHandler:(void (^) (KSSetPreferenceResponse *response))completionHandler;

/*!
* This method is used to make api call to get preference from server. It return instance of KSGetPreferenceResponse.
 
 @param completionHandler Handler to perform API action.
 */
- (void)getPreference:(void (^) (KSGetPreferenceResponse *response))completionHandler;

/*!
 * This method is used to make api call to retrieve all remote unlock readers from server. It return instance of KSRemoteUnlockReaderResponse.

 @param completionHandler Handler to perform API action.
 */
- (void)fetchRemoteReaders:(void (^) (KSRemoteUnlockReaderResponse *response))completionHandler;
/*!
 *This method is used to make api call to remotly unlock door.It return instance of KSUnlatchDoorResponse.

 @param remoteUnlockReader remote unlock reader to unlock.
 @param completionHandler Handler to perform API action.
 */
- (void)remoteUnlock:(KSRemoteUnlock *)remoteUnlockReader completionHandler:(void (^) (KSUnlatchDoorResponse *response))completionHandler;

/*!
 This method provides provision to refresh reader data.
 @param completionHandler  The completion handler, if provided, will be invoked on completion
 */
-(void)refreshReadersListWithCompletionHandler:(nullable void (^) (KSRefreshReadersResponse *response))completionHandler;


@end

NS_ASSUME_NONNULL_END
