//
//  KSServiceResponse.h
//  KastleSDK
//

#import <Foundation/Foundation.h>

@class KSError;

NS_ASSUME_NONNULL_BEGIN

/**
 Base class of all service response classes.
*/
@interface KSServiceResponse : NSObject

/**
Instance of KSError if any error occured, otherwise nil
 */
@property(nullable, nonatomic, readonly, strong) KSError *error;
/**
An String value to denote success message.
 */
@property(nullable, nonatomic, readonly, copy) NSString *successMessage;

-(instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
