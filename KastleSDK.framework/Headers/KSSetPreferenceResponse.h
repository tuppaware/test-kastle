/*!
 * API response class to retrieve response of setUserPreference API call.
 */

#import "KSServiceResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface KSSetPreferenceResponse : KSServiceResponse
@end

NS_ASSUME_NONNULL_END
