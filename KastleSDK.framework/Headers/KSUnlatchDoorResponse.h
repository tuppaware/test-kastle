/*!
 * API response class to retrieve response of remoteUnlock API call.
 */
#import "KSServiceResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface KSUnlatchDoorResponse : KSServiceResponse
@end

NS_ASSUME_NONNULL_END
