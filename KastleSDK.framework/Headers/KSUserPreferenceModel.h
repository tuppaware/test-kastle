/*!
 * Data model class, contains all user/enterprise preference configurations.
 * Host application receives object of KSUserPreferenceModel in data field of  getPreference api call.
 * Host application pass instance of KSUserPreferenceModel to setUserPreference method of SDK to save configuration on server.
 */
#import <Foundation/Foundation.h>
#import "KSBLEConstants.h"

NS_ASSUME_NONNULL_BEGIN
//Default RSSI values
static const NSInteger MaxUnlockDoorDistance = 4;
static const NSInteger MinUnlockDoorDistance = -4;

@interface KSUserPreferenceModel : NSObject
@property (nonatomic) BOOL soundEnable;

@property (nonatomic) BOOL vibrationEnable;

@property (nonatomic) BOOL morningNotificationEnable;

@property (nonatomic) BOOL handsFreeEnable;

@property (nonatomic) NSInteger unlockDoorDistance ;

@property (nonatomic) KSEnterpriseModeOfOperation enterpriseModeOfOperation;
@end

NS_ASSUME_NONNULL_END
