//
//  KSValidateUser.h
//  KastleSDK
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Data model class to be pass as parameter in validate user api request.
 */
@interface KSValidateUser : NSObject

/**
 Email Id of user
 */
@property (nonatomic, copy, readonly) NSString *email;
/**
 Mobile Number of user
 */
@property (nonatomic, copy, readonly) NSString *mobileNumber;

- (instancetype)initWithEmail:(NSString *)email mobileNumber:(NSString *)mobileNumber NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
