//
//  KSValidateUserResponse.h
//  KastleSDK
//

#import "KSServiceResponse.h"

@class KSValidateUserData;

@interface KSValidateUserData : NSObject

@end

NS_ASSUME_NONNULL_BEGIN

/**
 API response class to retrieve response of validateUser API call.
 */
@interface KSValidateUserResponse : KSServiceResponse
@property (nullable, strong, readonly) KSValidateUserData *data;
@end

NS_ASSUME_NONNULL_END
