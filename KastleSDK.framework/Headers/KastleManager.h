#import <Foundation/Foundation.h>
#import "KSServiceManagerInteractor.h"
#import "KSPushNotificationInteractor.h"
#import "KSLoggingInteractor.h"
#import "KSBLEDelegate.h"


NS_ASSUME_NONNULL_BEGIN
/*!
 KastleManger class is a manager class to create and to provide instance of SDK modules
 */
@interface KastleManager : NSObject

/*!
 This method will provide KastleManager singleton instance.
 @return KastleManager singleton instance
 */
+(instancetype)sharedManager;

/*!
Provides interactor to invoke all Kastle SDK exposed APIs .
 */
@property(nullable, nonatomic, strong, readonly) KSServiceManagerInteractor *serviceManagerInteractor;

/*!
Provides interactor to prepare pdf log file and retrieve it's path.
 */
@property(nullable, nonatomic, strong, readonly) KSLoggingInteractor *loggingInteractor;


/**
 Provides interactor to process pushnotification related operations
 */
@property(nullable, nonatomic, strong, readonly) KSPushNotificationInteractor *pushInteractor;


/*!
 Public property for getting BLE reader UI data - this delegate must be assigned a valid controller which conforms to KSReaderDataDelegate and will implement KSReaderDataDelegate protocol method.
 Its is also responsible for receiving failure callbacks such as bluetooth disable, location turned off etc.
 */
@property (nonatomic,weak) id <KSBLEDelegate> bleDelegate;

/*!
 This method will be used to unlock AppTap doors
 */
-(void)openDoorForIntentTypeReader:(KSBLEReader *)reader;

/*!
This method provides provision to refresh reader data.
 */
- (void)refreshReadersList DEPRECATED_MSG_ATTRIBUTE("Use ServiceManagerInteractor's refreshReadersListWithCompletionHandler: instead.");

@end

NS_ASSUME_NONNULL_END
