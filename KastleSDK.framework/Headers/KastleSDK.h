#import <UIKit/UIKit.h>

//! Project version number for KastleSDK.
FOUNDATION_EXPORT double KastleSDKVersionNumber;

//! Project version string for KastleSDK.
FOUNDATION_EXPORT const unsigned char KastleSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KastleSDK/PublicHeader.h>
//#import <KastleSDK/KastleManager.h>

#import "KastleSDK/KastleManager.h"
#import "KastleSDK/KSAdvanceSettingModel.h"
#import "KastleSDK/KSBLEDelegate.h"
#import "KastleSDK/KSBLEError.h"
#import "KastleSDK/KSBLEReader.h"
#import "KastleSDK/KSError.h"
#import "KastleSDK/KSGenerateTempPinResponse.h"
#import "KastleSDK/KSGetPreferenceResponse.h"
#import "KastleSDK/KSReader.h"
#import "KastleSDK/KSRegisterUser.h"
#import "KastleSDK/KSRegisterUserResponse.h"
#import "KastleSDK/KSRefreshReadersResponse.h"
#import "KastleSDK/KSRemoteUnlock.h"
#import "KastleSDK/KSRemoteUnlockReaderResponse.h"
#import "KastleSDK/KSServiceManagerInteractor.h"
#import "KastleSDK/KSServiceResponse.h"
#import "KastleSDK/KSSetPreferenceResponse.h"
#import "KastleSDK/KSUnlatchDoorResponse.h"
#import "KastleSDK/KSValidateUser.h"
#import "KastleSDK/KSValidateUserResponse.h"
#import "KastleSDK/KSAuthorizedUserResponse.h"
#import "KastleSDK/KSLoggingInteractor.h"
#import "KastleSDK/KSPushNotificationInteractor.h"

